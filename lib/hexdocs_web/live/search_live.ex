defmodule HexdocsWeb.SearchLive do
  use HexdocsWeb, :live_view

  alias Hexdocs.Resources.{Page, Package}
  alias Hexdocs.Jobs.CrawlPackageJob
  alias Hexdocs.Services.PackageCrawler

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     assign(socket,
       pages_grouped: %{},
       page_url: nil,
       insert_msg: "",
       existing_packages: Ash.read!(Package),
       sidebar_hidden: true,
       matching_package: nil
     )}
  end

  @impl true
  def handle_event("search_change", %{"key" => "Enter", "value" => value}, socket) do
    [potential_package | rest] = String.split(value)
    search_params = Enum.join(rest, " ")
    potential_package = String.downcase(potential_package)

    socket.assigns.existing_packages
    |> Enum.find(fn pkg ->
      String.contains?(String.downcase(pkg.name), potential_package)
    end)
    |> case do
      nil ->
        {:noreply, assign(socket, insert_msg: "Package not found in our database")}

      package ->
        url = "https://hexdocs.pm/#{package.name}/search.html?q=#{URI.encode(search_params)}"
        {:noreply, assign(socket, page_url: url, sidebar_hidden: true)}
    end
  end

  @impl true
  def handle_event("search_change", %{"value" => value} = params, socket) do
    pages =
      if value && value != "" do
        {:ok, results} = Page.search(value)
        group_search_results(results)
      else
        %{}
      end

    matching_package = find_package(value, socket.assigns.existing_packages)

    {:noreply,
     assign(socket,
       pages_grouped: pages,
       sidebar_hidden: false,
       matching_package: matching_package && matching_package.name
     )}
  end

  def handle_event("click_page_row", %{"url" => url}, socket) do
    {:noreply, assign(socket, page_url: url, sidebar_hidden: true)}
  end

  def handle_event("insert_package", %{"key" => "Enter", "value" => value}, socket) do
    socket =
      if PackageCrawler.exists?(value) do
        case Ash.create(Package, %{name: value}) do
          {:ok, _package} ->
            CrawlPackageJob.schedule_crawl(value)
            assign(socket, insert_msg: "package added, will index soon (10sec).")

          {:error, %{errors: [%{field: :name, message: "has already been taken"}]}} ->
            assign(socket, insert_msg: "package already listed.")

          other ->
            assign(socket, insert_msg: "failed to add package.")
        end
      else
        assign(socket, insert_msg: "package doesn't seem to exist.")
      end

    {:noreply, socket}
  end

  def handle_event("insert_package", _, socket) do
    {:noreply, socket}
  end

  def handle_event("toggle_sidebar", _, socket) do
    {:noreply, assign(socket, sidebar_hidden: !socket.assigns.sidebar_hidden)}
  end

  defp find_package(value, packages) do
    with true <- is_binary(value) and value != "",
         [potential_package | rest] <- String.split(value),
         true <- length(rest) > 0 do
      potential_package = String.downcase(potential_package)

      Enum.find(packages, fn pkg ->
        String.contains?(String.downcase(pkg.name), potential_package)
      end)
    else
      _ -> nil
    end
  end

  defp group_search_results(pages) do
    pages
    |> Enum.reduce(%{}, fn page, acc ->
      put_in(
        acc,
        [
          Access.key(page.package_name, %{}),
          Access.key(page.module_title, %{}),
          Access.key(page.sub_name, %{}),
          Access.key(page.anchor, %{})
        ],
        page
      )
    end)
  end
end
