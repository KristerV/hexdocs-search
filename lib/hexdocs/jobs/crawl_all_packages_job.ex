defmodule Hexdocs.Jobs.CrawlAllPackagesJob do
  use Oban.Worker,
    queue: :crawler,
    max_attempts: 1

  require Logger
  alias Hexdocs.Resources.Package
  alias Hexdocs.Jobs.CrawlPackageJob

  @impl Oban.Worker
  def perform(_job) do
    start_time = System.system_time(:second)
    Logger.info("Starting bulk crawl of all packages")

    packages = Ash.read!(Package)
    package_count = length(packages)

    for package <- packages do
      {:ok, _job} = CrawlPackageJob.schedule_crawl(package.name)
    end

    end_time = System.system_time(:second)
    duration = end_time - start_time
    Logger.info("Scheduled crawl for #{package_count} packages (took #{duration}s)")

    {:ok, package_count}
  end
end
