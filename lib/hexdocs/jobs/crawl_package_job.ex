defmodule Hexdocs.Jobs.CrawlPackageJob do
  use Oban.Worker,
    queue: :crawler,
    max_attempts: 3,
    unique: [
      # unique for 24h
      period: 60 * 60 * 24
    ]

  require Logger
  alias Hexdocs.Services.PackageCrawler
  alias Hexdocs.Resources.Page

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"package_name" => package_name}} = job) do
    start_time = System.system_time(:second)
    # destroy takes the actual record as an argument
    # case Page.delete_by_package_name(package_name) do
    #   {:ok, count} ->
    #     Logger.debug("Deleted #{count} existing pages for package: #{package_name}")

    #   {:error, error} ->
    #     Logger.error(
    #       "Failed to delete existing pages for package #{package_name}: #{inspect(error)}"
    #     )
    # end

    case PackageCrawler.run(package_name) do
      {:ok, _} = result ->
        end_time = System.system_time(:second)
        duration = end_time - start_time
        Logger.info("Successfully crawled package: #{package_name} (took #{duration}s)")
        result

      {:error, reason} = error ->
        end_time = System.system_time(:second)
        duration = end_time - start_time

        Logger.error(
          "Failed to crawl package #{package_name} after #{duration}s: #{inspect(reason)}"
        )

        error
    end
  end

  def schedule_crawl(package_name) do
    %{package_name: package_name}
    |> __MODULE__.new()
    |> Oban.insert()
  end
end
