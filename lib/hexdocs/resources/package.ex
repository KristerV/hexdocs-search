defmodule Hexdocs.Resources.Package do
  use Ash.Resource,
    data_layer: AshPostgres.DataLayer,
    domain: Hexdocs.Domain

  postgres do
    table "packages"
    repo Hexdocs.Repo
  end

  actions do
    default_accept [:name]
    defaults [:create, :read]
  end

  attributes do
    uuid_primary_key :id

    attribute :name, :string do
      allow_nil? false
    end

    timestamps()
  end

  identities do
    identity :unique_name, [:name]
  end
end
