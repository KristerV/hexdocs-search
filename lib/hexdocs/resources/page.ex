defmodule Hexdocs.Resources.Page do
  use Ash.Resource,
    data_layer: AshPostgres.DataLayer,
    domain: Hexdocs.Domain

  postgres do
    table "pages"
    repo Hexdocs.Repo
  end

  code_interface do
    define :delete_by_package_name, args: [:package_name]
    define :search, args: [:query]
  end

  actions do
    destroy :delete_by_package_name do
      argument :package_name, :string do
        allow_nil? false
      end

      filter expr(package.name == ^arg(:package_name))
    end

    default_accept [
      :url,
      :search_body,
      :module_title,
      :module_id,
      :sub_key,
      :anchor_id,
      :group,
      :anchor,
      :sub_name,
      :package_name,
      :package_id
    ]

    defaults [:create, :read]

    read :search do
      argument :query, :string do
        allow_nil? false
      end

      filter expr(
               fragment(
                 "? ilike ?",
                 search_body,
                 ("%" <> ^arg(:query) <> "%") |> string_split(" ") |> string_join("%")
               )
             )

      prepare build(
                sort: [package_name: :asc, module_title: :asc, sub_key: :asc, anchor_id: :asc],
                limit: 1000
              )
    end
  end

  attributes do
    uuid_primary_key :id
    attribute :package_name, :string, allow_nil?: false
    attribute :module_title, :string, allow_nil?: false
    attribute :sub_key, :string
    attribute :anchor_id, :string
    attribute :group, :string
    attribute :anchor, :string
    attribute :module_id, :string, allow_nil?: false
    attribute :search_body, :string, allow_nil?: false
    attribute :sub_name, :string
    attribute :url, :string, allow_nil?: false

    timestamps()
  end

  relationships do
    belongs_to :package, Hexdocs.Resources.Package
  end
end
