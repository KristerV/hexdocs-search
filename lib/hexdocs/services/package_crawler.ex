defmodule Hexdocs.Services.PackageCrawler do
  require Logger

  @base_url "https://hexdocs.pm/"

  def exists?(name) do
    name
    |> name_to_url()
    |> Req.get()
    |> case do
      {:ok, %{status: 404}} -> false
      {:ok, %{status: 200}} -> true
      {:ok, %{status: 301}} -> true
      {:ok, %{status: _}} -> false
      {:error, _} -> false
    end
  end

  def run(package_name) do
    url = name_to_url(package_name)

    with {:ok, %{body: body}} <- Req.get(url, max_redirects: 3),
         {:ok, document} <- Floki.parse_document(body),
         {:ok, redirect_path} <- extract_meta_refresh_url(document),
         redirect_url <- join_url_path(url, redirect_path),
         {:ok, %{body: body}} <- Req.get(redirect_url),
         {:ok, document} <- Floki.parse_document(body),
         {:ok, scripts} <- extract_script_urls(document, package_name),
         {:ok, _} <- process_scripts(scripts) do
      {:ok, package_name}
    else
      error ->
        Logger.error("Failed to crawl package #{package_name}: #{inspect(error)}")
        error
    end
  end

  defp extract_meta_refresh_url(document) do
    case Floki.find(document, "meta[http-equiv=\"refresh\"]") do
      [{"meta", attrs, _}] ->
        case List.keyfind(attrs, "content", 0) do
          {_, content} ->
            case Regex.run(~r/\d+;\s*url=(.+)/, content) do
              [_, url] -> {:ok, String.trim(url)}
              _ -> {:error, :no_url_in_content}
            end

          _ ->
            {:error, :no_content_attr}
        end

      _ ->
        {:error, :no_meta_refresh}
    end
  end

  defp process_scripts(scripts) do
    results =
      scripts
      |> Task.async_stream(
        fn {package_name, script_url} ->
          Hexdocs.Services.DocsParser.run(package_name, script_url)
        end,
        ordered: false,
        timeout: :timer.minutes(10)
      )
      |> Enum.to_list()

    if Enum.all?(results, &match?({:ok, {:ok, _}}, &1)) do
      {:ok, :processed}
    else
      {:error, :script_processing_failed}
    end
  end

  defp name_to_url(name) do
    @base_url
    |> URI.parse()
    |> URI.merge(name)
    |> to_string()
  end

  defp join_url_path(url, redirect_path) do
    uri = URI.parse(url)
    new_path = Path.join(uri.path || "", redirect_path)
    %{uri | path: new_path} |> to_string()
  end

  defp extract_script_urls(document, package_name) do
    document
    |> Floki.find("script")
    |> Floki.attribute("src")
    |> Enum.filter(&String.starts_with?(&1, "dist/sidebar_items"))
    |> Enum.map(fn url ->
      script_url = make_url(@base_url, "#{package_name}/#{url}")
      {package_name, script_url}
    end)
    |> then(fn results ->
      if Enum.any?(results) do
        {:ok, results}
      else
        {:error, :no_scripts_found}
      end
    end)
  end

  defp make_url(base_url, path) do
    base_url
    |> URI.parse()
    |> URI.merge(path)
    |> to_string()
  end
end
