defmodule Hexdocs.Services.DocsParser do
  require Logger

  def run(package_name, script_url) do
    start_time = System.system_time(:second)
    Logger.debug("Starting docs parse for package: #{package_name}")

    with {:ok, %{body: body}} <- Req.get(script_url),
         {:ok, data} <- parse_sidebar_items(body),
         {:ok, pages} <- parse_nodes(data, package_name) do
      end_time = System.system_time(:second)
      duration = end_time - start_time

      Logger.info(
        "Successfully parsed #{length(pages)} pages for #{package_name} (took #{duration}s)"
      )

      {:ok, pages}
    else
      error ->
        end_time = System.system_time(:second)
        duration = end_time - start_time

        Logger.error(
          "Failed to parse docs for #{package_name} after #{duration}s: #{inspect(error)}"
        )

        {:error, :parse_failed}
    end
  end

  defp parse_sidebar_items(body) do
    body
    |> String.trim_leading("sidebarNodes=")
    |> Jason.decode()
  end

  defp parse_nodes(sidebar_items, package) do
    pages =
      for {_type, items} <- sidebar_items,
          item <- items,
          page <- parse_item(item, package) do
        page
      end

    {:ok, pages}
  end

  defp parse_item(
         %{"group" => group, "id" => id, "title" => title, "headers" => headers},
         package
       ) do
    group_data = %{name: group, id: id, title: title}

    for header <- headers do
      node_to_page(group_data, %{}, header, package)
    end
  end

  defp parse_item(
         %{"group" => group, "id" => id, "title" => title, "nodeGroups" => node_groups},
         package
       ) do
    group_data = %{name: group, id: id, title: title}

    for %{"key" => key, "name" => name, "nodes" => nodes} <- node_groups,
        node <- nodes do
      node_to_page(group_data, %{key: key, name: name}, node, package)
    end
  end

  defp parse_item(%{"group" => group, "id" => id, "title" => title}, package) do
    group_data = %{name: group, id: id, title: title}
    [node_to_page(group_data, %{}, %{}, package)]
  end

  defp parse_item(%{"functions" => functions, "id" => id, "title" => title}, package) do
    group_data = %{name: "functions", id: id, title: title}
    node_group = %{key: "Function", name: "Function"}

    for fun <- functions do
      node_to_page(group_data, node_group, fun, package)
    end
  end

  defp parse_item(item, package) do
    Logger.error("Unknown item format for #{package}: #{inspect(Map.keys(item))}")
    []
  end

  defp node_to_page(group, node_group, anchor, package) do
    data = [
      group: group.name,
      module_title: group.title,
      module_id: group.id,
      sub_key: Map.get(node_group, :key, "Section"),
      sub_name: Map.get(node_group, :name, "Section"),
      anchor: Map.get(anchor, "anchor", "top"),
      anchor_id: Map.get(anchor, "id", "top"),
      package_name: package
    ]

    # Lookup the package record
    {:ok, pkg} = Ash.get(Hexdocs.Resources.Package, name: package)
    data = Keyword.put(data, :package_id, pkg.id)

    search_body =
      data
      |> Keyword.take([:package, :module_title, :sub_key, :anchor, :anchor_id])
      |> Keyword.values()
      |> Enum.filter(& &1)
      |> Enum.join(" ")

    page_attrs =
      data
      |> Map.new()
      |> Map.put(:search_body, search_body)
      |> Map.put(
        :url,
        "https://hexdocs.pm/#{package}/#{group.id}.html##{Map.get(anchor, "anchor", "")}"
      )

    {:ok, page} = Ash.create(Hexdocs.Resources.Page, page_attrs)
    page
  end
end
