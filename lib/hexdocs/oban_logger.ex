defmodule Hexdocs.ObanLogger do
  @moduledoc """
  Oban doesn't have a logger by default. Define one so errors end up in console.
  """
  require Logger

  def handle_event([:oban, :job, :start], _measure, meta, _) do
    Logger.debug(
      "[Oban] :started #{meta.worker}. #{inspect(meta.args)}. attempt: #{meta.attempt}"
    )
  end

  def handle_event([:oban, :job, :exception], _measure, meta, _) do
    Logger.error(
      "[Oban] :execption #{meta.worker}. #{inspect(meta.args)}\nmessage: #{inspect(meta.error)}\nstacktrace:\n#{Exception.format_stacktrace(meta.stacktrace)}."
    )
  end

  def handle_event([:oban, :job, :stop], _measure, meta, _) do
    Logger.debug("[Oban] :stop #{meta.worker}. #{inspect(meta.args)}")
  end
end
