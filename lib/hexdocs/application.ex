defmodule Hexdocs.Application do
  require Logger
  require Ecto.Query
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children =
      [
        HexdocsWeb.Telemetry,
        Hexdocs.Repo,
        {DNSCluster, query: Application.get_env(:hexdocs, :dns_cluster_query) || :ignore},
        {Phoenix.PubSub, name: Hexdocs.PubSub},
        {Oban, Application.fetch_env!(:hexdocs, Oban)},
        {Finch, name: Hexdocs.Finch},
        HexdocsWeb.Endpoint
      ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Hexdocs.Supervisor]
    result = Supervisor.start_link(children, opts)
    setup_oban()

    if Application.get_env(:hexdocs, :env) != :test do
      init_default_packages()
    end

    result
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HexdocsWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp setup_oban do
    Oban.Job
    |> Ecto.Query.where(state: "executing")
    |> Oban.cancel_all_jobs()

    oban_events = [[:oban, :job, :start], [:oban, :job, :stop], [:oban, :job, :exception]]
    :telemetry.attach_many("oban-logger", oban_events, &Hexdocs.ObanLogger.handle_event/4, [])
  end

  defp handle_event([:oban, :job, event], measurements, metadata, _) do
    duration = System.convert_time_unit(measurements.duration, :native, :millisecond)
    prefix = "[hexdocs] "

    case event do
      :start ->
        Logger.info("#{prefix}Starting #{metadata.worker} with args #{inspect(metadata.args)}")

      :stop ->
        Logger.info("#{prefix}Completed #{metadata.worker} in #{duration}ms")

      :exception ->
        Logger.error(
          "#{prefix}Failed #{metadata.worker} in #{duration}ms\n#{inspect(metadata.error)}"
        )
    end
  end

  defp init_default_packages do
    Task.start(fn ->
      for package <- Application.get_env(:hexdocs, :default_packages) do
        case Ash.create(Hexdocs.Resources.Package, %{name: package}) do
          {:ok, _} ->
            Hexdocs.Jobs.CrawlPackageJob.schedule_crawl(package)

          {:error, _} ->
            :ok
        end
      end
    end)
  end
end
