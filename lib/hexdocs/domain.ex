defmodule Hexdocs.Domain do
  use Ash.Domain

  resources do
    resource Hexdocs.Resources.Package
    resource Hexdocs.Resources.Page
  end
end
